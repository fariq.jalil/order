export const SET_USER = "SET_USER";
export const ASSIGN_ORDER = "ASSIGN_ORDER";
export const UNASSIGN_ORDER = "UNASSIGN_ORDER";
export const UPDATE_ORDER = "UPDATE_ORDER";
