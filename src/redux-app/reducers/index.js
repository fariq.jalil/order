import { combineReducers } from "redux";

import bikers from "redux-app/reducers/bikers";
import orders from "redux-app/reducers/orders";
import user from "redux-app/reducers/users";

export default combineReducers({
  bikers,
  orders,
  user
});
