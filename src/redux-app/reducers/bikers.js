import bikers from "mocks/bikers.json";

const INITIAL_STATE = bikers;

const bikersReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

export default bikersReducer;
