import { SET_USER } from "redux-app/actiontypes";

const userReducer = (state = { id: 0, name: "Manager" }, action) => {
  switch (action.type) {
    case SET_USER:
      return { id: action.id, name: action.name };
    default:
      return state;
  }
};

export default userReducer;
