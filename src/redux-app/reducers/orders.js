import orders from "mocks/orders.json";
import {
  ASSIGN_ORDER,
  UNASSIGN_ORDER,
  UPDATE_ORDER
} from "redux-app/actiontypes";

const INITIAL_STATE = orders;

const ordersReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ASSIGN_ORDER:
      return state.map(order => {
        if (order.id === action.orderid) {
          return {
            id: action.orderid,
            status: "ASSIGNED",
            biker: action.bikerid,
            pickupTime: null,
            deliverTime: null
          };
        }
        return order;
      });
    case UNASSIGN_ORDER:
      return state.map(order => {
        if (order.id === action.orderid) {
          return {
            id: action.orderid,
            status: "WAITING",
            biker: null,
            pickupTime: null,
            deliverTime: null
          };
        }
        return order;
      });
    case UPDATE_ORDER:
      return state.map(order => {
        if (order.id === action.orderid) {
          return {
            id: action.orderid,
            status: action.status,
            biker: action.bikerid,
            pickupTime: action.pickupTime,
            deliverTime: action.deliverTime
          };
        }
        return order;
      });
    default:
      return state;
  }
};

export default ordersReducer;
