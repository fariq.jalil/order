import React from "react";
import { Tabs, Tab } from "react-bootstrap";

import ManagerOrders from "./Orders";
import ManagerBikers from "./Bikers";

const Manager = () => {
  return (
    <Tabs defaultActiveKey="orders" id="manager-tab">
      <Tab eventKey="orders" title="Orders">
        <ManagerOrders />
      </Tab>
      <Tab eventKey="bikers" title="Bikers">
        <ManagerBikers />
      </Tab>
    </Tabs>
  );
};

export default Manager;
