import React from "react";
import { useSelector } from "react-redux";
import { Col, Row } from "react-bootstrap";

const Bikers = () => {
  const { bikers, orders } = useSelector(state => state);

  return (
    <>
      <h3>Bikers</h3>
      <Row>
        {bikers.map((biker, idx) => {
          const filterOrders = orders.filter(order => order.biker === biker.id);

          return (
            <Col key={idx} md={4} className="card">
              <div className="title">
                <span>{biker.name}</span>
              </div>

              <span className="body">
                {filterOrders.length} Orders Assigned:
              </span>

              {filterOrders.map((order, idx) => (
                <p
                  key={idx}
                  className={`body 
                    ${
                      order.status === "DELIVERED"
                        ? "row-green"
                        : order.status === "PICKEDUP"
                        ? "row-yellow"
                        : ""
                    }`}
                >
                  #{order.id} - {order.status}{" "}
                  {order.status === "PICKEDUP"
                    ? order.pickupTime
                    : order.status === "DELIVERED" && order.deliverTime}
                </p>
              ))}
            </Col>
          );
        })}
      </Row>
    </>
  );
};

export default Bikers;
