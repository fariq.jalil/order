import React, { Fragment } from "react";
import { Dropdown, Table } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";

import { ASSIGN_ORDER, UNASSIGN_ORDER } from "redux-app/actiontypes";

const Orders = () => {
  const { bikers, orders } = useSelector(state => state);
  const dispatch = useDispatch();

  const assignOrder = (orderid, bikerid) => {
    dispatch({
      type: ASSIGN_ORDER,
      orderid,
      bikerid
    });
  };

  const unassignOrder = orderid => {
    dispatch({
      type: UNASSIGN_ORDER,
      orderid
    });
  };

  return (
    <>
      <h3>Orders</h3>
      <Table>
        <thead>
          <th>#</th>
          <th>Status</th>
          <th>Biker</th>
          <th>Time</th>
          <th />
        </thead>
        <tbody>
          {orders.map((order, idx) => (
            <tr
              key={idx}
              className={
                order.status === "DELIVERED"
                  ? "row-green"
                  : order.status === "WAITING"
                  ? "row-red"
                  : ""
              }
            >
              <td>{order.id}</td>
              <td>{order.status}</td>
              <td>
                {bikers.find(biker => biker.id === order.biker) &&
                  bikers.find(biker => biker.id === order.biker).name}
              </td>
              <td>
                {order.status === "PICKEDUP"
                  ? order.pickupTime
                  : order.status === "DELIVERED"
                  ? order.deliverTime
                  : "-- : --"}
              </td>
              <td>
                {order.status !== "DELIVERED" && (
                  <Dropdown>
                    <Dropdown.Toggle
                      variant="secondary"
                      id="dropdown-basic"
                    ></Dropdown.Toggle>

                    <Dropdown.Menu>
                      <Dropdown.Item disabled>
                        {order.biker ? "Re-assign to" : "Assign to"}
                      </Dropdown.Item>
                      {bikers
                        .filter(biker => biker.id !== order.biker)
                        .map((biker, idx) => (
                          <Fragment key={idx}>
                            <Dropdown.Item
                              onClick={() => assignOrder(order.id, biker.id)}
                            >
                              {biker.name}
                            </Dropdown.Item>
                          </Fragment>
                        ))}
                      {order.biker && (
                        <>
                          <Dropdown.Divider />
                          <Dropdown.Item
                            onClick={() => unassignOrder(order.id)}
                          >
                            Un-assign
                          </Dropdown.Item>
                        </>
                      )}
                    </Dropdown.Menu>
                  </Dropdown>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

export default Orders;
