import React from "react";
import { Navbar, NavDropdown } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";

import { SET_USER } from "redux-app/actiontypes";

const MyNavbar = () => {
  const { bikers, user } = useSelector(state => state);
  const dispatch = useDispatch();

  const onClick = (id, name) => {
    dispatch({
      type: SET_USER,
      id,
      name
    });
  };

  return (
    <Navbar bg="dark" variant="dark">
      <Navbar.Brand>Order</Navbar.Brand>
      <NavDropdown
        title={user.name}
        className="nav-dropdown-title"
        id="collasible-nav-dropdown"
      >
        <NavDropdown.Item onClick={key => onClick(0, "Manager")}>
          Manager
        </NavDropdown.Item>

        {bikers.map((biker, idx) => (
          <NavDropdown.Item
            key={idx}
            onClick={key => onClick(biker.id, `Biker - ${biker.name}`)}
          >
            Biker - {biker.name}
          </NavDropdown.Item>
        ))}
      </NavDropdown>
    </Navbar>
  );
};

export default MyNavbar;
