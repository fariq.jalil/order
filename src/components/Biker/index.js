import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Table } from "react-bootstrap";

import TimeForm from "components/TimeForm";
import { UPDATE_ORDER } from "redux-app/actiontypes";

const Bikers = () => {
  const { orders, user } = useSelector(state => state);
  const dispatch = useDispatch();

  const onPickup = (e, orderid, bikerid) => {
    e.preventDefault();
    const input = document.getElementById(`pickup-${orderid}`);
    dispatch({
      type: UPDATE_ORDER,
      orderid,
      status: "PICKEDUP",
      bikerid,
      pickupTime: input.value,
      deliverTime: null
    });
  };

  const onDeliver = (e, orderid, bikerid, pickupTime) => {
    e.preventDefault();
    const input = document.getElementById(`deliver-${orderid}`);

    dispatch({
      type: UPDATE_ORDER,
      orderid,
      status: "DELIVERED",
      bikerid,
      pickupTime,
      deliverTime: input.value
    });
  };

  return (
    <>
      <h3>Orders</h3>
      <Table>
        <thead>
          <th>#</th>
          <th>Status</th>
          <th>Picked Up Time</th>
          <th>Delivered Time</th>
          <th />
        </thead>
        <tbody>
          {orders.filter(order => order.biker === user.id).length > 0 ? (
            orders
              .filter(order => order.biker === user.id)
              .map((order, idx) => {
                return (
                  <tr
                    key={idx}
                    className={order.status === "DELIVERED" ? "row-green" : ""}
                  >
                    <td>{order.id}</td>
                    <td>{order.status}</td>
                    <td>{order.pickupTime || "-- : --"}</td>
                    <td>{order.deliverTime || "-- : --"}</td>
                    <td>
                      {order.status === "ASSIGNED" && (
                        <TimeForm
                          id={`pickup-${order.id}`}
                          onSubmit={e => onPickup(e, order.id, order.biker)}
                          inputName="pickup"
                          buttonVariant="warning"
                          buttonName="Picked Up"
                        />
                      )}
                      {order.status === "PICKEDUP" && (
                        <TimeForm
                          id={`deliver-${order.id}`}
                          onSubmit={e =>
                            onDeliver(
                              e,
                              order.id,
                              order.biker,
                              order.pickupTime
                            )
                          }
                          inputName="deliver"
                          buttonVariant="success"
                          buttonName="Delivered"
                        />
                      )}
                    </td>
                  </tr>
                );
              })
          ) : (
            <tr>
              <td colSpan={5}>No order has been assigned</td>
            </tr>
          )}
        </tbody>
      </Table>
    </>
  );
};

export default Bikers;
