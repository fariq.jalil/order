import React from "react";
import { Button, Col, Form } from "react-bootstrap";

const TimeForm = ({
  id,
  inputName,
  buttonVariant,
  buttonName,
  onSubmit,
  disabled
}) => {
  return (
    <Form onSubmit={onSubmit}>
      <Form.Row>
        <Col>
          <Form.Control
            id={id}
            name={inputName}
            type="time"
            required
            disabled={disabled}
          />
        </Col>
        <Col>
          <Button type="submit" variant={buttonVariant} disabled={disabled}>
            {buttonName}
          </Button>
        </Col>
      </Form.Row>
    </Form>
  );
};

export default TimeForm;
