import React from "react";
import { useSelector } from "react-redux";

import Bikers from "components/Biker";
import Manager from "components/Manager/index";
import Layout from "components/Layout";

const Root = () => {
  const { user } = useSelector(state => state);

  return <Layout>{user.id > 0 ? <Bikers /> : <Manager />}</Layout>;
};

export default Root;
